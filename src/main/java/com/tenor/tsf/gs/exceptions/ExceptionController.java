package com.tenor.tsf.gs.exceptions;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionController {

	@ExceptionHandler({RuntimeException.class})
    public ResponseEntity<Object> handleRunTimeException(RuntimeException e) {
    	Error error=new Error(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,LocalDateTime.now());
        return new ResponseEntity<Object>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @ExceptionHandler({NullPointerException.class})
    public ResponseEntity<Object> handleNPException(NullPointerException e) {
    	Error error=new Error(e.getMessage(), HttpStatus.BAD_REQUEST,LocalDateTime.now());
        return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<Object> handleIllegaleArgException(IllegalArgumentException e) {
    	Error error=new Error(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,LocalDateTime.now());
        return new ResponseEntity<Object>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<Object> handleNotFoundException(NotFoundException e) {
    	Error error=new Error(e.getMsg(), HttpStatus.BAD_REQUEST,LocalDateTime.now());
        return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler({RequiredFieldException.class})
    public ResponseEntity<Object> handleRequiredFieldException(RequiredFieldException e) {
    	Error error=new Error(e.getMsg(), HttpStatus.BAD_REQUEST,LocalDateTime.now());
        return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);

    }
    
    @ExceptionHandler({AllreadyReservedException.class})
    public ResponseEntity<Object> handleAllreadyReservedException(AllreadyReservedException e) {
    	Error error=new Error(e.getMsg(), HttpStatus.BAD_REQUEST,LocalDateTime.now());
        return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler({DateExpection.class})
    public ResponseEntity<Object> handleDateException(DateExpection e) {
    	Error error=new Error(e.getMessage(), HttpStatus.BAD_REQUEST,LocalDateTime.now());
        return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
    }

	
}
