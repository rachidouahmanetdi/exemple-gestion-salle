package com.tenor.tsf.gs.repository;

import org.springframework.data.repository.CrudRepository;

import com.tenor.tsf.gs.entity.Utilisateur;

public interface UtilisateurRepository<P> extends CrudRepository<Utilisateur, Long>{

}
